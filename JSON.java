//This is free and unencumbered software released into the public domain.
//
//Anyone is free to copy, modify, publish, use, compile, sell, or
//distribute this software, either in source code form or as a compiled
//binary, for any purpose, commercial or non-commercial, and by any
//means.
//
//In jurisdictions that recognize copyright laws, the author or authors
//of this software dedicate any and all copyright interest in the
//software to the public domain. We make this dedication for the benefit
//of the public at large and to the detriment of our heirs and
//successors. We intend this dedication to be an overt act of
//relinquishment in perpetuity of all present and future rights to this
//software under copyright law.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
//IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
//ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//OTHER DEALINGS IN THE SOFTWARE.
//
//For more information, please refer to <http://unlicense.org/>
package publicdomain;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;

/**
 * This is a simple, lightweight JSON parsing and writing API. It parses JSON
 * files according to
 * <a href="https://www.json.org/json-en.html">https://www.json.org/json-en.html</a>
 * and converts the values to Java types. Furthermore it accepts Java types and
 * stores them such that they themselves can be written to a file or stream.
 * <br><br>
 * As by JSON specification (key-value) Objects as well as Strings, Numbers,
 * "true", "false", null, and arrays are all valid JSON and can be parsed with
 * this implementation.
 * <br> The implementation follows, when parsing, the context-free grammar of
 * JSON which means that duplicate keys will not throw and error and the last
 * parsed value for any given duplicate key will be used. When writing, no
 * duplicate keys will be produced, since the internal Java HashMap will only
 * hold the latest value for any given key.
 * <br><br>
 * Every JSON instance can be validated by a subset of constraints of the
 * <a href="https://json-schema.org/">https://json-schema.org/</a>
 * specification. JSON schema is a JSON validation scheme defined itself in JSON
 * <br><br>
 * <b>What is special in this implementation?</b>
 * <br>
 * <ul>
 * <li>
 * More white space characters are allowed:
 * <br>JSON specification allows "", u0020, u000A, u000D, u0009
 * <br>This implementation allows every Character defined by
 * {@link java.lang.Character#isWhitespace}
 * </li>
 * <li>
 * Types are converted to Java number types:
 * <br>JSON specification allows arbitrary length integers and arbitrary
 * precision floating point values.
 * <br>This implementation can read Java signed Integers and automatically
 * convert them to a Java signed Long, when Integer.MAX_VALUE is exceeded.<b>No
 * automatic conversion to BigInteger</b>. All read floating point values will
 * be converted to a Java double.
 * </li>
 * <li>
 * Upon insertion of a Java float, this implementation will automatically
 * convert it to a double.
 * </li>
 * </ul>
 */
public final class JSON implements Iterable<Object> {

	private final static char EOF = (char) -1;

	/**
	 * Whether or not forward slashes will be escaped in any JSON String
	 * written. (Default: false). The JSON specification allows the escape of
	 * forward slashes "/" as "\/" but it is optional.
	 */
	public static boolean ESCAPE_FORWARD_SLASHES = false;

	/**
	 * The type a JSON instance can have. One of:
	 * <ul>
	 * <li>JSON_OBJECT</li>
	 * <li>JSON_ARRAY</li>
	 * <li>JSON_STRING</li>
	 * <li>JSON_NUMBER</li>
	 * <li>TRUE</li>
	 * <li>FALSE</li>
	 * <li>NULL</li>
	 * </ul>
	 */
	public static enum JSONVALUE_TYPE {
		JSON_OBJECT,
		JSON_ARRAY,
		JSON_STRING,
		JSON_NUMBER,
		TRUE,
		FALSE,
		NULL
	}

	private static enum LITERAL {
		TRUE(JSONVALUE_TYPE.TRUE, "true", true),
		FALSE(JSONVALUE_TYPE.FALSE, "false", false),
		NULL(JSONVALUE_TYPE.NULL, "null", null);

		public final JSONVALUE_TYPE type;
		public final String name;
		public final Object value;

		private LITERAL(JSONVALUE_TYPE _type, String _literal, Object _literalValue) {
			type = _type;
			name = _literal;
			value = _literalValue;
		}

		private static LITERAL FROM_TYPE(JSONVALUE_TYPE _type) {
			switch (_type) {
				case FALSE:
					return (FALSE);
				case TRUE:
					return (TRUE);
				case NULL:
					return (NULL);
				default:
					throw new IllegalArgumentException("Not a valid literal");
			}
		}
	}

	private final static class JSONVALUE {

		private static String JAVASTRING2JSONSTRING(final String _string) {
			String ret = _string.replace("\\", "\\\\");
			if (ESCAPE_FORWARD_SLASHES) {
				ret = ret.replace("/", "\\/");
			}
			ret = ret.replace("\"", "\\\"");
			ret = ret.replace("\b", "\\b");
			ret = ret.replace("\f", "\\f");
			ret = ret.replace("\n", "\\n");
			ret = ret.replace("\r", "\\r");
			ret = ret.replace("\t", "\\t");
			for (char c = 0x0; c < 0x20; c++) {
				if (ret.indexOf(c) != -1) {
					String hex = ("0000" + Integer.toHexString(c));
					ret = ret.replace(Character.toString(c), "\\u" + hex.substring(hex.length() - 4, hex.length()));
				}
			}
			return (ret);
		}

		private final JSONVALUE_TYPE type;
		private final Object value;

		private JSONVALUE(final JSONVALUE_TYPE _type, final Object _object) {
			type = _type;
			value = _object;
		}

		private JSONVALUE(final LITERAL _literal) {
			type = _literal.type;
			value = _literal.value;
		}

		private JSONVALUE(final Object _value) {
			if (_value == null) {
				type = LITERAL.NULL.type;
				value = LITERAL.NULL.value;
			} else if (_value instanceof Boolean) {
				type = (boolean) _value ? LITERAL.TRUE.type : LITERAL.FALSE.type;
				value = (boolean) _value ? LITERAL.TRUE.value : LITERAL.FALSE.value;
			} else if (_value instanceof String) {
				type = JSONVALUE_TYPE.JSON_STRING;
				value = _value;
			} else if (_value instanceof Float) {
				type = JSONVALUE_TYPE.JSON_NUMBER;
				value = (double) (float) _value;
			} else if (_value instanceof Number) {
				type = JSONVALUE_TYPE.JSON_NUMBER;
				value = _value;
			} else if (_value instanceof JSON) {
				//Copy reference if _value.root is OBJECT or ARRAY
				type = ((JSON) _value).root_.type;
				value = ((JSON) _value).root_.value;
			} else {
				throw new IllegalArgumentException("Not a serializable value (" + _value.getClass().getName() + ")");
			}
		}

		private Object toValue() {
			switch (type) {
				case JSON_ARRAY:
				case JSON_OBJECT:
					return (new JSON(this));
				default:
					return (value);
			}
		}

		@Override
		public String toString() {
			switch (type) {
				case TRUE:
				case FALSE:
				case NULL:
				case JSON_NUMBER:
				case JSON_STRING:
					return (String.valueOf(value) + " (" + type.name() + ")");
				case JSON_ARRAY:
					return (type.name() + " " + ((ArrayList) value).size() + " entries");
				case JSON_OBJECT:
					return (type.name() + " " + ((HashMap) value).size() + " entries");
			}

			throw new IllegalStateException("Invalid JSONVALUE");
		}

		@Override
		public boolean equals(final Object _other) {
			if (_other == null || !(_other instanceof JSONVALUE)) {
				return (false);
			}

			JSONVALUE other = (JSONVALUE) _other;

			if (type != other.type) {
				return (false);
			}

			switch (type) {
				case NULL:
				case TRUE:
				case FALSE:
				case JSON_NUMBER:
					return (value == other.value);
				case JSON_STRING:
					return (value.equals(other.value));
				case JSON_ARRAY:
					return (((ArrayList<JSONVALUE>) value).equals(other.value));
				case JSON_OBJECT:
					return (((HashMap<String, JSONVALUE>) value).equals(other.value));
			}

			return (false);
		}

		private boolean equalsValue(final Object _other) {
			switch (type) {
				case NULL:
				case TRUE:
				case FALSE:
				case JSON_NUMBER:
					if (_other instanceof Float) {
						return ((double) value == (double) (float) _other);
					}
					return (value == _other);
				case JSON_STRING:
					return (value.equals(_other));
				case JSON_ARRAY:
					return (toValue().equals(_other));
				case JSON_OBJECT:
					return (toValue().equals(_other));
			}
			return (false);
		}

		@Override
		public int hashCode() {
			int hash = 5;
			hash = 61 * hash + Objects.hashCode(type);
			hash = 61 * hash + Objects.hashCode(value);
			return hash;
		}

		private JSONVALUE copy() {
			switch (type) {
				case NULL:
					return (new JSONVALUE(LITERAL.NULL));
				case TRUE:
					return (new JSONVALUE(LITERAL.TRUE));
				case FALSE:
					return (new JSONVALUE(LITERAL.FALSE));
				case JSON_STRING:
					return (new JSONVALUE(JSONVALUE_TYPE.JSON_STRING, ((String) value)));
				case JSON_NUMBER:
					return (new JSONVALUE(JSONVALUE_TYPE.JSON_NUMBER, value));
				case JSON_ARRAY:
					ArrayList<JSONVALUE> newarray = new ArrayList<>();
					for (JSONVALUE oldvalue : (ArrayList<JSONVALUE>) value) {
						newarray.add(oldvalue.copy());
					}
					return (new JSONVALUE(JSONVALUE_TYPE.JSON_ARRAY, newarray));
				case JSON_OBJECT:
					HashMap<String, JSONVALUE> newobject = new HashMap<>();
					for (Map.Entry<String, JSONVALUE> oldentry : ((HashMap<String, JSONVALUE>) value).entrySet()) {
						newobject.put(oldentry.getKey(), oldentry.getValue().copy());
					}
					return (new JSONVALUE(JSONVALUE_TYPE.JSON_OBJECT, newobject));
			}
			throw new IllegalStateException("Invalid JSONVALUE");
		}

		private String json_toString(final boolean _whitespace, final int _depth) {
			switch (type) {
				case NULL:
				case FALSE:
				case TRUE:
				case JSON_NUMBER:
					return (String.valueOf(value));
				case JSON_STRING:
					return ("\"" + JAVASTRING2JSONSTRING((String) value) + "\"");
				case JSON_ARRAY: {
					String ret = "[";
					boolean first = true;
					boolean obj = false;
					for (JSONVALUE v : (ArrayList<JSONVALUE>) value) {
						if (!first) {
							ret += ",";
							if (_whitespace) {
								ret += " ";
							}
						}
						if (!_whitespace || v.type != JSONVALUE_TYPE.JSON_OBJECT) {
							ret += v.json_toString(_whitespace, _depth);
						} else {
							obj = true;
							ret += "\n";
							for (int i = 0; i < _depth + 1; i++) {
								ret += "\t";
							}
							ret += v.json_toString(_whitespace, _depth + 2);
						}
						first = false;
					}
					if (_whitespace && obj) {
						ret += "\n";
						for (int i = 0; i < _depth; i++) {
							ret += "\t";
						}
					}
					ret += "]";
					return (ret);
				}
				case JSON_OBJECT: {
					String ret = "{";
					boolean first = true;
					for (Map.Entry<String, JSONVALUE> entry : ((HashMap<String, JSONVALUE>) value).entrySet()) {
						if (!first) {
							ret += ",";
						}
						if (_whitespace) {
							ret += "\n";
						}
						if (_whitespace) {
							for (int i = 0; i < _depth; i++) {
								ret += "\t";
							}
						}
						ret += "\"" + entry.getKey() + "\":";
						if (_whitespace) {
							ret += " ";
						}
						if (entry.getValue().type == JSONVALUE_TYPE.JSON_OBJECT) {
							ret += entry.getValue().json_toString(_whitespace, _depth + 1);
						} else {
							ret += entry.getValue().json_toString(_whitespace, _depth);
						}
						first = false;
					}
					if (_whitespace) {
						ret += "\n";
					}
					if (_whitespace) {
						for (int i = 0; i < _depth - 1; i++) {
							ret += "\t";
						}
					}
					ret += "}";
					return (ret);
				}
			}

			return ("");
		}
	}

	private static class RETURN_TUPLE {

		private final JSONVALUE returnValue;
		private final char returnChar;

		public RETURN_TUPLE(JSONVALUE _returnValue, char _returnChar) {
			returnValue = _returnValue;
			returnChar = _returnChar;
		}

	}

	@FunctionalInterface
	private static interface CHAR_SUPPLIER {

		char nextChar();
	}

	private JSON(final JSONVALUE _value) {
		root_ = _value;
	}

	//==========================================================================
	//#                                                                        #
	//#    Provide different constructors to cover different sources of JSON.  #
	//#   Each constructor calls the respective init method. The init methods  #
	//#   clear the current JSON and parse the given source. JSON class is     #
	//#                         therefor mutable.                              #
	//#                                                                        #
	//==========================================================================
	private JSONVALUE root_;

	/**
	 * Creates a new empty JSON. The type of this will be
	 * {@link JSONVALUE_TYPE#NULL}.
	 * <br>
	 * However with the methods
	 * <br>{@link JSON#json_add(java.lang.Object)},
	 * <br>{@link JSON#json_add(java.lang.String, java.lang.Object)},
	 * <br>{@link JSON#json_add(int, java.lang.Object)}
	 * <br>this JSON will be automatically converted to an Key-Value object or
	 * array.
	 */
	public JSON() {
		json_init();
	}

	/**
	 * Creates a new "empty" JSON, but sets the type according to the specified
	 * parameter.
	 * <ul>
	 * <li>If the _type is a LITERAL, the literal value is used.</li>
	 * <li>If the _type is a {@link JSONVALUE_TYPE#JSON_NUMBER}, the value 0 is
	 * used.</li>
	 * <li>If the _type is a {@link JSONVALUE_TYPE#JSON_STRING}, the empty
	 * String ("") is used.</li>
	 * <li>If the _type is an {@link JSONVALUE_TYPE#JSON_ARRAY}, a new empty
	 * array will be created.</li>
	 * <li>If the _type is an {@link JSONVALUE_TYPE#JSON_OBJECT}, a new empty
	 * object will be created.</li>
	 * </ul>
	 *
	 * @param _type The type of this JSON
	 */
	public JSON(JSONVALUE_TYPE _type) {
		json_init(_type);
	}

	/**
	 * Creates a new JSON and deep copies all values of the specified JSON. (No
	 * references will be reused)
	 *
	 * @param _other The JSON to be copied
	 */
	public JSON(final JSON _other) {
		json_init(_other);
	}

	/**
	 * Creates a new JSON and parses the specified file
	 *
	 * @param _file The file to be parsed
	 * @throws IOException If file reading fails
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public JSON(final File _file) throws IOException, IllegalStateException {
		json_init(_file);
	}

	/**
	 * Creates a new JSON and parses the specified input stream
	 *
	 * @param _input The stream to be parsed
	 * @throws IOException If file reading fails
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public JSON(final InputStream _input) throws IOException, IllegalStateException {
		json_init(_input);
	}

	/**
	 * Creates a new JSON and parses the specified String
	 *
	 * @param _input The String to be parsed
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public JSON(final String _input) throws IllegalStateException {
		json_init(_input);
	}

	/**
	 * Creates a new JSON and parses the specified input
	 *
	 * @param _input The bytes to be parsed
	 * @param _encoding The encoding of the specified bytes
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public JSON(final byte[] _input, final Charset _encoding) throws IllegalStateException {
		json_init(_input, _encoding);
	}

	/**
	 * Creates a new JSON and parses the specified input
	 *
	 * @param _input The bytes to be parsed in a ByteBuffer
	 * @param _encoding The encoding of the specified bytes
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public JSON(final ByteBuffer _input, final Charset _encoding) throws IllegalStateException {
		json_init(_input, _encoding);
	}

	/**
	 * Creates a new JSON and parses the specified input
	 *
	 * @param _input The chars to be parsed
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public JSON(final CharBuffer _input) throws IllegalStateException {
		json_init(_input);
	}

	/**
	 * Cleans this JSON and sets the type to {@link JSONVALUE_TYPE#NULL}.
	 *
	 * @return This JSON
	 */
	public final JSON json_init() {
		root_ = new JSONVALUE(LITERAL.NULL);
		return (this);
	}

	/**
	 * Cleans this JSON and sets the type to the specified
	 * {@link JSONVALUE_TYPE}.
	 *
	 * <ul>
	 * <li>If the _type is a LITERAL, the literal value is used.</li>
	 * <li>If the _type is a {@link JSONVALUE_TYPE#JSON_NUMBER}, the value 0 is
	 * used.</li>
	 * <li>If the _type is a {@link JSONVALUE_TYPE#JSON_STRING}, the empty
	 * String ("") is used.</li>
	 * <li>If the _type is an {@link JSONVALUE_TYPE#JSON_ARRAY}, a new empty
	 * array will be created.</li>
	 * <li>If the _type is an {@link JSONVALUE_TYPE#JSON_OBJECT}, a new empty
	 * object will be created.</li>
	 * </ul>
	 *
	 * @param _type The {@link JSONVALUE_TYPE} to initializes this JSON to
	 * @return This JSON
	 */
	public final JSON json_init(JSONVALUE_TYPE _type) {
		switch (_type) {
			case NULL:
			case FALSE:
			case TRUE:
				root_ = new JSONVALUE(LITERAL.FROM_TYPE(_type));
				break;
			case JSON_NUMBER:
				root_ = new JSONVALUE(0);
				break;
			case JSON_STRING:
				root_ = new JSONVALUE("");
				break;
			case JSON_ARRAY:
				root_ = new JSONVALUE(_type, new ArrayList<JSONVALUE>());
				break;
			case JSON_OBJECT:
				root_ = new JSONVALUE(_type, new HashMap<String, JSONVALUE>());
				break;
		}
		return (this);
	}

	/**
	 * Deep copies the values of the given JSON to this JSON. (No references
	 * will be reused)
	 *
	 * @param _other The JSON to be copied
	 * @return This JSON
	 */
	public final JSON json_init(final JSON _other) {
		root_ = _other.root_.copy();
		return (this);
	}

	/**
	 * Creates a new JSON and deep copies all values of this JSON to the new
	 * JSON. (No references will be reused)
	 *
	 * @return The new JSON
	 */
	public final JSON copy() {
		return (new JSON(this));
	}

	//==========================================================================
	//#                                                                        #
	//#                    Stream based initialization                         #
	//#                                                                        #
	//==========================================================================
	/**
	 * Parses the given input and replaces the current JSON with the result.
	 *
	 * @param _json The JSON to be parsed
	 * @return This JSON
	 * @throws IOException If reading fails
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public final JSON json_init(final File _json) throws IOException, IllegalStateException {
		try (BufferedReader br = new BufferedReader(new FileReader(_json))) {
			json_init(br);
		}
		return (this);
	}

	/**
	 * Parses the given input and replaces the current JSON with the result.
	 *
	 * @param _json The JSON to be parsed
	 * @return This JSON
	 * @throws IOException If reading fails
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public final JSON json_init(final InputStream _json) throws IOException, IllegalStateException {
		return (json_init(new BufferedReader(new InputStreamReader(_json))));
	}

	/**
	 * Parses the given input and replaces the current JSON with the result.
	 *
	 * @param _json The JSON to be parsed
	 * @return This JSON
	 * @throws IOException If reading fails
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public final JSON json_init(final BufferedReader _json) throws IOException, IllegalStateException {
		json_init();
		try {
			root_ = parse_root(() -> {
				try {
					return ((char) _json.read());
				} catch (IOException ex) {
					throw new UncheckedIOException(ex);
				}
			});
		} catch (UncheckedIOException ex) {
			throw ex.getCause();
		}
		return (this);
	}

	//==========================================================================
	//#                                                                        #
	//#                 CharBuffer based initialization                        #
	//#                                                                        #
	//==========================================================================
	/**
	 * Parses the given input and replaces the current JSON with the result.
	 *
	 * @param _json The JSON to be parsed
	 * @return This JSON
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public final JSON json_init(final String _json) {
		return (json_init(CharBuffer.wrap(_json)));
	}

	/**
	 * Parses the given input and replaces the current JSON with the result.
	 *
	 * @param _json The JSON to be parsed
	 * @param _encoding The encoding of the specified bytes
	 * @return This JSON
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public final JSON json_init(final byte[] _json, final Charset _encoding) {
		return (json_init(_encoding.decode(ByteBuffer.wrap(_json))));
	}

	/**
	 * Parses the given input and replaces the current JSON with the result.
	 *
	 * @param _json The JSON to be parsed
	 * @param _encoding The encoding of the specified bytes
	 * @return This JSON
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public final JSON json_init(final ByteBuffer _json, final Charset _encoding) {
		json_init(_encoding.decode(_json));
		_json.rewind();
		return (this);
	}

	/**
	 * Parses the given input and replaces the current JSON with the result.
	 *
	 * @param _json The JSON to be parsed
	 * @return This JSON
	 * @throws IllegalStateException If the parsing fails due to a syntax error
	 */
	public final JSON json_init(final CharBuffer _json) {
		json_init();
		root_ = parse_root(() -> {
			if (!_json.hasRemaining()) {
				return (EOF);
			}
			return (_json.get());
		});
		return (this);
	}

	//==========================================================================
	//#                                                                        #
	//#                       JSON parsing methods                             #
	//#                                                                        #
	//#           Parse the JSON context free language according to            #
	//#                  https://www.json.org/json-en.html                     #
	//#                                                                        #
	//#      We parse character by character (PDA like) using the internal     #
	//#      CHAR_SUPPLIER class. This wraps streams and buffers to a common   #
	//#      proxy, which will provide EOF when the stream or buffer is exh-   #
	//#      austed.                                                           #
	//#                                                                        #
	//==========================================================================
	private JSONVALUE parse_root(final CHAR_SUPPLIER _input) {

		char character;

		do {
			character = _input.nextChar();
		} while (Character.isWhitespace(character));

		if (character == EOF) {
			return (new JSONVALUE(LITERAL.NULL));
		}

		RETURN_TUPLE value = parse_value(character, _input);

		if (value.returnChar != EOF && !Character.isWhitespace(value.returnChar)) {
			throw new IllegalStateException("Illegal JSON: '" + value.returnValue.value + "'");
		}

		char c;
		while ((c = _input.nextChar()) != EOF) {
			if (!Character.isWhitespace(c)) {
				throw new IllegalStateException("EOF expected. Got '" + Character.toString(c) + "' (" + (int) c + ", 0x" + Integer.toHexString(c) + ") in: '" + value.returnValue.value + "'");
			}
		}

		return (value.returnValue);
	}

	private RETURN_TUPLE parse_value(final char _firstChar, final CHAR_SUPPLIER _input) {

		final JSONVALUE_TYPE type = parse_valueType(_firstChar);

		switch (type) {
			case NULL:
				return (new RETURN_TUPLE(parse_literal(LITERAL.NULL, _firstChar, _input.nextChar(), _input.nextChar(), _input.nextChar()), EOF));

			case TRUE:
				return (new RETURN_TUPLE(parse_literal(LITERAL.TRUE, _firstChar, _input.nextChar(), _input.nextChar(), _input.nextChar()), EOF));

			case FALSE:
				return (new RETURN_TUPLE(parse_literal(LITERAL.FALSE, _firstChar, _input.nextChar(), _input.nextChar(), _input.nextChar(), _input.nextChar()), EOF));

			case JSON_STRING:
				StringParseState stringState = new StringParseState();

				char c;
				while (stringState.state != StringParseState.STATE.DONE) {
					c = _input.nextChar();
					if (c == EOF) {
						throw new IllegalStateException("Unexpected EOF while parsing String: '" + stringState.builder.toString() + "'");
					}
					stringState.parseNext((char) c);
				}

				return (new RETURN_TUPLE(new JSONVALUE(JSONVALUE_TYPE.JSON_STRING, stringState.builder.toString()), EOF));

			case JSON_NUMBER:
				NumberParseState numberState = new NumberParseState((char) _firstChar);

				char d;
				while (true) {
					d = _input.nextChar();

					if (!numberState.validCharacter((char) d)) {
						if (numberState.state == NumberParseState.STATE.LEADING_MINUS) {
							throw new IllegalStateException("Illegal number '-'");
						}
						if (numberState.state == NumberParseState.STATE.EXPONENT || numberState.state == NumberParseState.STATE.EXPONENT_SIGN) {
							throw new IllegalStateException("Missing number exponent '" + numberState.builder.toString() + "'");
						}
						break;
					}

					numberState.parseNext((char) d);
				}

				switch (numberState.type) {
					case INTEGER:
						return (new RETURN_TUPLE(new JSONVALUE(JSONVALUE_TYPE.JSON_NUMBER, Integer.parseInt(numberState.builder.toString())), d));

					case LONG:
						return (new RETURN_TUPLE(new JSONVALUE(JSONVALUE_TYPE.JSON_NUMBER, Long.parseLong(numberState.builder.toString())), d));

					case DOUBLE:
						return (new RETURN_TUPLE(new JSONVALUE(JSONVALUE_TYPE.JSON_NUMBER, Double.parseDouble(numberState.builder.toString())), d));
				}

			case JSON_ARRAY:
				final ArrayList<JSONVALUE> array = new ArrayList<>();

				char a = _input.nextChar();
				while (true) {

					if (a == EOF) {
						throw new IllegalStateException("Unexpected EOF while parsing array.");
					}

					while (Character.isWhitespace(a)) {
						a = _input.nextChar();
					}

					if (a == ']') {
						break;
					}

					RETURN_TUPLE rTuple = parse_value(a, _input);

					array.add(rTuple.returnValue);
					a = rTuple.returnChar;

					if (a == EOF) {
						a = _input.nextChar();
					}

					while (Character.isWhitespace(a)) {
						a = _input.nextChar();
					}

					if (a != ',' && a != ']') {
						throw new IllegalStateException("Expected ',' or ']' while parsing array. Got '" + Character.toString(a) + "' (" + (int) a + ", 0x" + Integer.toHexString(a) + "). Last value '" + rTuple.returnValue + "'.");
					}

					if (a == ',') {
						do {
							a = _input.nextChar();
						} while (Character.isWhitespace(a));

						if (a == ']') {
							throw new IllegalStateException("Unexpected ']' while parsing array. Last value '" + rTuple.returnValue + "'");
						}
					}
				}

				return (new RETURN_TUPLE(new JSONVALUE(JSONVALUE_TYPE.JSON_ARRAY, array), EOF));

			case JSON_OBJECT:
				final HashMap<String, JSONVALUE> object = new HashMap<>();

				char o = _input.nextChar();
				while (true) {

					if (o == EOF) {
						throw new IllegalStateException("Unexpected EOF while parsing object.");
					}

					while (Character.isWhitespace(o)) {
						o = _input.nextChar();
					}

					if (o == '}') {
						break;
					}

					RETURN_TUPLE keyTuple = parse_value(o, _input);

					if (keyTuple.returnValue.type != JSONVALUE_TYPE.JSON_STRING) {
						throw new IllegalStateException("Unexpected key while parsing object. Key has to be String got: '" + keyTuple.returnValue + "'");
					}

					o = keyTuple.returnChar;

					if (o == EOF) {
						o = _input.nextChar();
					}

					while (Character.isWhitespace(o)) {
						o = _input.nextChar();
					}

					if (o != ':') {
						throw new IllegalStateException("Expected ':' while parsing object. Got '" + Character.toString(o) + "' (" + (int) o + ", 0x" + Integer.toHexString(o) + "). Key '" + keyTuple.returnValue + "'.");
					}

					do {
						o = _input.nextChar();
					} while (Character.isWhitespace(o));

					RETURN_TUPLE valueTuple = parse_value(o, _input);

					object.put((String) keyTuple.returnValue.value, valueTuple.returnValue);
					o = valueTuple.returnChar;

					if (o == EOF) {
						o = _input.nextChar();
					}

					while (Character.isWhitespace(o)) {
						o = _input.nextChar();
					}

					if (o != ',' && o != '}') {
						throw new IllegalStateException("Expected ',' or '}' while parsing object. Got '" + Character.toString(o) + "' (" + (int) o + ", 0x" + Integer.toHexString(o) + "). Key '" + keyTuple.returnValue + "'.");
					}

					if (o == ',') {
						do {
							o = _input.nextChar();
						} while (Character.isWhitespace(o));

						if (o == '}') {
							throw new IllegalStateException("Unexpected '}' while parsing object. Key '" + keyTuple.returnValue + "'.");
						}
					}
				}

				return (new RETURN_TUPLE(new JSONVALUE(JSONVALUE_TYPE.JSON_OBJECT, object), EOF));

			default:
				throw new IllegalStateException("Parsing failed. Invalid JSONVALUE.");
		}
	}

	private JSONVALUE_TYPE parse_valueType(final char _character) {

		switch (_character) {
			case '{':
				return (JSONVALUE_TYPE.JSON_OBJECT);
			case '[':
				return (JSONVALUE_TYPE.JSON_ARRAY);
			case '"':
				return (JSONVALUE_TYPE.JSON_STRING);
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '-':
				return (JSONVALUE_TYPE.JSON_NUMBER);
			case 't':
				return (JSONVALUE_TYPE.TRUE);
			case 'f':
				return (JSONVALUE_TYPE.FALSE);
			case 'n':
				return (JSONVALUE_TYPE.NULL);
		}

		throw new IllegalStateException("Illegal JSONVALUE starting character: '" + Character.toString(_character) + "' (" + (int) _character + ", 0x" + Integer.toHexString(_character) + ")");
	}

	private JSONVALUE parse_literal(final LITERAL _literal, final char... _chars) {
		for (int i = 0; i < _literal.name.length(); i++) {
			if (_literal.name.charAt(i) != _chars[i]) {
				throw new IllegalStateException("Unexpected character in " + _literal + "-literal: '" + Character.toString((char) _chars[i]) + "' (" + (int) _chars[i] + ", 0x" + Integer.toHexString(_chars[i]) + ")");
			}
		}
		return (new JSONVALUE(_literal.type, _literal.value));
	}

	private static class StringParseState {

		public static enum STATE {
			CODEPOINT,
			ESCAPE,
			UNICODE,
			DONE
		}

		private final StringBuilder builder = new StringBuilder();
		private STATE state = STATE.CODEPOINT;
		private String unicodeBuffer = "";

		private void parseNext(final char _character) {
			switch (state) {
				case DONE:
					throw new IllegalStateException("Unexpected String parsing call: '" + builder.toString() + "'");
				case CODEPOINT:

					if (_character < 0x20) {
						throw new IllegalStateException("Illegal control character '0x" + Integer.toHexString(_character) + "' in String: '" + builder.toString() + "'");
					}

					switch (_character) {
						case '"':
							state = STATE.DONE;
							break;
						case '\\':
							state = STATE.ESCAPE;
							break;
						default:
							builder.append(_character);
							break;
					}
					break;
				case ESCAPE:
					switch (_character) {
						case '"':
						case '\\':
						case '/':
							builder.append(_character);
							state = STATE.CODEPOINT;
							break;

						case 'b':
							builder.append('\b');
							state = STATE.CODEPOINT;
							break;
						case 'f':
							builder.append('\f');
							state = STATE.CODEPOINT;
							break;
						case 'n':
							builder.append('\n');
							state = STATE.CODEPOINT;
							break;
						case 'r':
							builder.append('\r');
							state = STATE.CODEPOINT;
							break;
						case 't':
							builder.append('\t');
							state = STATE.CODEPOINT;
							break;

						case 'u':
							state = STATE.UNICODE;
							unicodeBuffer = "";
							break;

						default:
							throw new IllegalStateException("Illegal escape sequence '\\" + Character.toString(_character) + "' (0x" + Integer.toHexString(_character) + ") in String: '" + builder.toString() + "'");
					}
					break;
				case UNICODE:
					if ((_character >= 0x30 && _character <= 0x39) || (_character >= 0x65 && _character <= 0x46) || (_character >= 0x61 && _character <= 0x66)) {
						//Only allow 0-9 A-F a-f
						unicodeBuffer += Character.toString(_character);
						if (unicodeBuffer.length() == 4) {
							builder.append(Character.toString((char) Integer.parseInt(unicodeBuffer, 16)));
							state = STATE.CODEPOINT;
						}
					} else {
						throw new IllegalStateException("Illegal unicode escape sequence '\\u" + unicodeBuffer + "' in String: '" + builder.toString() + "'");
					}
					break;
			}
		}
	}

	private static class NumberParseState {

		public final static String JAVA_INT_MAX = "2147483647";
		public final static String JAVA_INT_MIN = "-2147483648";

		public static enum STATE {
			LEADING_ZERO,
			LEADING_MINUS,
			LEADING_DIGIT,
			DIGIT,
			DECIMAL_POINT,
			FRACTION,
			EXPONENT,
			EXPONENT_SIGN,
			EXPONENT_DIGIT
		}

		public static enum JAVA_NUMBER_TYPE {
			INTEGER,
			LONG,
			DOUBLE
		}

		private final StringBuilder builder = new StringBuilder();
		private final boolean negative;
		private STATE state;
		private JAVA_NUMBER_TYPE type = JAVA_NUMBER_TYPE.INTEGER;

		public NumberParseState(final char _character) {
			builder.append(_character);

			switch (_character) {
				case '-':
					state = STATE.LEADING_MINUS;
					negative = true;
					break;
				case '0':
					state = STATE.LEADING_ZERO;
					negative = false;
					break;
				default:
					state = STATE.LEADING_DIGIT;
					negative = false;
					break;
			}
		}

		private boolean validCharacter(final char _character) {
			if (_character >= 0x30 && _character <= 0x39) {
				return (state != STATE.LEADING_ZERO);
			}

			if (_character == '.') {
				return (state == STATE.DIGIT || state == STATE.LEADING_DIGIT || state == STATE.LEADING_ZERO);
			}

			if (_character == 'e' || _character == 'E') {
				return (state == STATE.DIGIT || state == STATE.LEADING_DIGIT || state == STATE.LEADING_ZERO || state == STATE.FRACTION);
			}

			if (_character == '+' || _character == '-') {
				return (state == STATE.EXPONENT);
			}

			return (false);
		}

		private void parseNext(final char _character) {
			if (_character >= 0x30 && _character <= 0x39) {
				switch (state) {
					case LEADING_MINUS:
						if (_character == '0') {
							state = STATE.LEADING_ZERO;
						}
					//Fall through:
					case LEADING_DIGIT:
						builder.append(_character);
						state = STATE.DIGIT;
						break;

					case DIGIT:
						//Check if the current number exceeds java integer
						checkType(_character);
					//Fall through:
					case FRACTION:
					case EXPONENT_DIGIT:
						builder.append(_character);
						break;

					case DECIMAL_POINT:
						builder.append(_character);
						state = STATE.FRACTION;
						break;

					case EXPONENT:
					case EXPONENT_SIGN:
						builder.append(_character);
						state = STATE.EXPONENT_DIGIT;
						break;

					default:
						throw new IllegalStateException("Illegal digit '" + Character.toString(_character) + "' in parsing number: '" + builder.toString() + "'");
				}

				return;
			}

			switch (_character) {
				case '.':
					if (state == STATE.LEADING_ZERO || state == STATE.LEADING_DIGIT || state == STATE.DIGIT) {
						builder.append(_character);
						state = STATE.DECIMAL_POINT;
						type = JAVA_NUMBER_TYPE.DOUBLE;
					} else {
						throw new IllegalStateException("Illegal decimal point in parsing number: '" + builder.toString() + "'");
					}
					break;
				case 'E':
				case 'e':
					if (state == STATE.DIGIT || state == STATE.LEADING_DIGIT || state == STATE.LEADING_ZERO || state == STATE.FRACTION) {
						builder.append(_character);
						state = STATE.EXPONENT;
						type = JAVA_NUMBER_TYPE.DOUBLE;
					} else {
						throw new IllegalStateException("Illegal number exponent in parsing number: '" + builder.toString() + "'");
					}
					break;
				case '+':
				case '-':
					if (state == STATE.EXPONENT) {
						builder.append(_character);
						state = STATE.EXPONENT_SIGN;
						type = JAVA_NUMBER_TYPE.DOUBLE;
					} else {
						throw new IllegalStateException("Illegal number exponent sign in parsing number: '" + builder.toString() + "'");
					}
					break;
			}
		}

		private void checkType(final char _character) {

			if (type != JAVA_NUMBER_TYPE.INTEGER) {
				return;
			}

			String MAX_NUM = JAVA_INT_MAX;
			if (negative) {
				MAX_NUM = JAVA_INT_MIN;
			}

			if (builder.length() + 1 > MAX_NUM.length()) {
				type = JAVA_NUMBER_TYPE.LONG;
			} else if (builder.length() + 1 == MAX_NUM.length()) {
				boolean decided = false;

				for (int i = negative ? 1 : 0; i < builder.length(); i++) {

					if (builder.charAt(i) > MAX_NUM.charAt(i)) {
						type = JAVA_NUMBER_TYPE.LONG;
						decided = true;
						break;
					} else if (builder.charAt(i) < MAX_NUM.charAt(i)) {
						decided = true;
						break;
					}

				}

				if (!decided && _character > MAX_NUM.charAt(MAX_NUM.length() - 1)) {
					type = JAVA_NUMBER_TYPE.LONG;
				}
			}
		}
	}

	//==========================================================================
	//#                                                                        #
	//#                     JSON schema validation                             #
	//#                                                                        #
	//#           Implement validation methods based on the ideas of           #
	//#                      https://json-schema.org/                          #
	//#                                                                        #
	//#       Notice that this implementation does not contain all schema      #
	//#               attributes at the time of this writing.                  #
	//#       This means: THIS IMPLEMENTATION DOES NOT FOLLOW AN OFFICIAL      #
	//#             SPECIFICATION DRAFT DUE TO MISSING COMPLETENESS            #
	//#                                                                        #
	//==========================================================================
	/**
	 * Validates this JSON based on the restrictions given in the specified
	 * validator. This follows a small subset of the
	 * <a href="https://json-schema.org/">https://json-schema.org/</a>
	 * definition.
	 *
	 * @param _validator The validator to use
	 * @return <i>True</i>, if the validation succeeded. <i>False</i> otherwise.
	 */
	public final boolean json_validate(final JSON _validator) {
		return (validate(root_, _validator));
	}

	private boolean validate(final JSONVALUE _object, final JSON _validator) {
		switch (_validator.type_get()) {
			case TRUE:
			case FALSE:
				return ((boolean) _validator.json_get());
			case JSON_OBJECT:
				break;
			default:
				throw new IllegalArgumentException("Illegal validator. Not a valid JSON validator (" + _validator.root_.toString() + ")");
		}

		if (_validator.json_size() == 0) {
			return (true);
		}

		if (!_validator.json_containsKey("type")) {
			return (true);
		}

		HashSet<String> allowedTypes = new HashSet<>();

		switch (_validator.json_getType("type")) {
			case JSON_STRING:
				allowedTypes.add(_validator.json_getString("type"));
				break;
			case JSON_ARRAY:
				for (String s : _validator.json_getArray("type").<String>iteratorCast()) {
					allowedTypes.add(s);
				}
				break;
			default:
				throw new IllegalArgumentException("Unexpected validator keyword 'type' must be a String or a String array, is: (" + _validator.json_get("type") + ")");
		}

		String matchingType = validate_matchType(_object, allowedTypes);

		if (matchingType == null) {
			return (false);
		}

		if (!validate_type(_object, _validator, matchingType)) {
			return (false);
		}

		return (true);
	}

	private boolean validate_type(final JSONVALUE _object, final JSON _validator, final String _type) {
		switch (_type) {
			case "string":
				if (_validator.json_containsKey("enum")) {
					boolean enumFound = false;
					for (String test : _validator.json_getArray("enum").<String>iteratorCast()) {
						if (_object.equalsValue(test)) {
							enumFound = true;
							break;
						}
					}
					if (!enumFound) {
						return (false);
					}
				}
				if (_validator.json_containsKey("minLength")) {
					int minLength = _validator.json_getInt("minLength");
					if (((String) _object.toValue()).length() < minLength) {
						return (false);
					}
				}
				if (_validator.json_containsKey("maxLength")) {
					int minLength = _validator.json_getInt("maxLength");
					if (((String) _object.toValue()).length() > minLength) {
						return (false);
					}
				}
				return (true);
			case "integer":
				if (_validator.json_containsKey("minimum")) {
					int minimum = _validator.json_getInt("minimum");
					if (((int) _object.toValue()) < minimum) {
						return (false);
					}
				}
				if (_validator.json_containsKey("maximum")) {
					int maximum = _validator.json_getInt("maximum");
					if (((int) _object.toValue()) > maximum) {
						return (false);
					}
				}
				return (true);
			case "number":
				if (_validator.json_containsKey("minimum")) {
					double minimum;
					if (_validator.json_get("minimum") instanceof Double) {
						minimum = _validator.json_getDouble("minimum");
					} else {
						minimum = (double) _validator.json_getInt("minimum");
					}
					if (((double) _object.toValue()) < minimum) {
						return (false);
					}
				}
				if (_validator.json_containsKey("maximum")) {
					double maximum;
					if (_validator.json_get("maximum") instanceof Double) {
						maximum = _validator.json_getDouble("maximum");
					} else {
						maximum = (double) _validator.json_getInt("maximum");
					}
					if (((double) _object.toValue()) > maximum) {
						return (false);
					}
				}
				return (true);
			case "array":
				if (_validator.json_containsKey("minItems")) {
					int minItems = _validator.json_getInt("minItems");
					if (((JSON) _object.toValue()).json_size() < minItems) {
						return (false);
					}
				}
				if (_validator.json_containsKey("maxItems")) {
					int maxItems = _validator.json_getInt("maxItems");
					if (((JSON) _object.toValue()).json_size() > maxItems) {
						return (false);
					}
				}
				if (_validator.json_containsKey("items")) {
					for (JSONVALUE item : (ArrayList<JSONVALUE>) _object.value) {
						if (!validate(item, _validator.json_getObject("items"))) {
							return (false);
						}
					}
				}
				return (true);
			case "object":
				HashMap<String, JSONVALUE> objectEntries = ((HashMap<String, JSONVALUE>) _object.value);
				if (_validator.json_containsKey("properties")) {
					for (Object entryobj : _validator.json_getObject("properties")) {
						Map.Entry<String, JSON> entry = (Map.Entry<String, JSON>) entryobj;
						if (!objectEntries.containsKey(entry.getKey())) {
							continue;
						}
						if (!validate(objectEntries.get(entry.getKey()), entry.getValue())) {
							return (false);
						}
					}
				}

				if (_validator.json_containsKey("required")) {
					for (String key : _validator.json_getArray("required").<String>iteratorCast()) {
						if (!objectEntries.containsKey(key)) {
							return (false);
						}
					}
				}
				return (true);
		}
		return (true);
	}

	private String validate_matchType(final JSONVALUE _object, final HashSet<String> _allowedTypes) {
		for (String s : _allowedTypes) {
			switch (s) {
				case "string":
					if (_object.type == JSONVALUE_TYPE.JSON_STRING) {
						return (s);
					}
					break;
				case "integer":
					if (_object.type == JSONVALUE_TYPE.JSON_NUMBER && _object.value instanceof Integer) {
						return (s);
					}
					break;
				case "number":
					if (_object.type == JSONVALUE_TYPE.JSON_NUMBER) {
						return (s);
					}
					break;
				case "object":
					if (_object.type == JSONVALUE_TYPE.JSON_OBJECT) {
						return (s);
					}
					break;
				case "array":
					if (_object.type == JSONVALUE_TYPE.JSON_ARRAY) {
						return (s);
					}
					break;
				case "boolean":
					if (_object.type == JSONVALUE_TYPE.TRUE || _object.type == JSONVALUE_TYPE.FALSE) {
						return (s);
					}
					break;
				case "null":
					if (_object.type == JSONVALUE_TYPE.NULL) {
						return (s);
					}
					break;
			}
		}

		return (null);
	}

	//==========================================================================
	//#                                                                        #
	//#                      Public General API                                #
	//#                                                                        #
	//==========================================================================
	@Override
	public boolean equals(final Object _other) {
		if (_other == null || !(_other instanceof JSON)) {
			return (false);
		}

		return (root_.equals(((JSON) _other).root_));
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 23 * hash + Objects.hashCode(root_);
		return hash;
	}

	/**
	 * Returns the {@link JSONVALUE_TYPE} of this JSON.
	 *
	 * @return The type of this JSON
	 */
	public final JSONVALUE_TYPE type_get() {
		return (root_.type);
	}

	//==========================================================================
	//#                                                                        #
	//#                       Public Access API                                #
	//#                                                                        #
	//==========================================================================
	/**
	 * Returns the Java value of this JSON. This will be either a native Java
	 * type e.g. String, int, double, boolean, null OR a JSON instance, if this
	 * JSON is either a JSON array or a JSON object.
	 *
	 * @return The value of this JSON
	 */
	public final Object json_get() {
		switch (root_.type) {
			case JSON_ARRAY:
			case JSON_OBJECT:
				return (this);
			default:
				return (root_.value);
		}
	}

	@Override
	public Iterator<Object> iterator() {
		switch (root_.type) {
			case NULL:
				return (Collections.emptyIterator());
			case JSON_ARRAY:
				return (new Iterator<Object>() {
					final ArrayList<JSONVALUE> values = (ArrayList<JSONVALUE>) root_.value;
					int id = 0;

					@Override
					public boolean hasNext() {
						return (id < values.size());
					}

					@Override
					public Object next() {
						return (values.get(id++).toValue());
					}
				});
			case JSON_OBJECT:
				return (new Iterator<Object>() {
					final LinkedList<String> keys = new LinkedList<>(((HashMap<String, JSONVALUE>) root_.value).keySet());
					int id = 0;

					@Override
					public boolean hasNext() {
						return (id < keys.size());
					}

					@Override
					public Map.Entry<String, Object> next() {
						HashMap<String, JSONVALUE> map = ((HashMap<String, JSONVALUE>) root_.value);
						String key = keys.get(id++);
						return (new AbstractMap.SimpleEntry<>(key, map.get(key).toValue()));
					}
				});
			default:
				return (Collections.<Object>singleton(root_.toValue()).iterator());
		}
	}

	/**
	 * Returns an iterable (used in for each loops) where every object will be
	 * cast to the specified type T.
	 *
	 * @param <T> The type to cast each element to
	 * @return An iterable with every object cast to T
	 */
	public <T> Iterable<T> iteratorCast() {
		return (new Iterable<T>() {
			@Override
			public Iterator<T> iterator() {
				return ((Iterator<T>) JSON.this.iterator());
			}
		});
	}

	/**
	 * Returns an iterable (used in for each loops) where every each key-value
	 * pair of a JSON object will be iterated over.
	 * <br>
	 * This is basicly a shorthand for
	 * {@code json.<Map.Entry<String, Object>>iteratorCast() }
	 *
	 * @return An iterable over this JSON object
	 * @throws ClassCastException If this JSON is neither a JSON object nor null
	 */
	public Iterable<Map.Entry<String, Object>> iteratorObject() throws ClassCastException {
		if (root_.type != JSONVALUE_TYPE.JSON_OBJECT && root_.type != JSONVALUE_TYPE.NULL) {
			throw new ClassCastException("Can not use iteratorObject(). This JSON is not an object.");
		}
		return (iteratorCast());
	}

	//#                                                                        #
	//#                                                                        #
	//+++++++++++++++++++++++++++Value == Object++++++++++++++++++++++++++++++++
	/**
	 * Adds the given value to this JSON with the given key. This assumes that
	 * this JSON is either a JSON object or null. If this JSON is null, this
	 * JSON will be converted to a new empty JSON object.
	 *
	 * @param _key The key of the new value
	 * @param _value The new value
	 * @return This JSON
	 * @throws ClassCastException If this JSON is neither a JSON object nor null
	 */
	public final JSON json_add(final String _key, final Object _value) throws ClassCastException {
		if (root_.type != JSONVALUE_TYPE.JSON_OBJECT) {
			if (root_.type != JSONVALUE_TYPE.NULL) {
				throw new ClassCastException("JSON is not an object. JSON is '" + root_.toString() + "'");
			}
			root_ = new JSONVALUE(JSONVALUE_TYPE.JSON_OBJECT, new HashMap<String, JSONVALUE>());
		}

		JSONVALUE newvalue = new JSONVALUE(_value);
		((HashMap<String, JSONVALUE>) root_.value).put(_key, newvalue);

		return (this);
	}

	/**
	 * Returns the {@link JSONVALUE_TYPE} of the value in this JSON with the
	 * specified key. This assumes that this JSON is a JSON object.
	 *
	 * @param _key The key of the value to check the type of
	 * @return The {@link JSONVALUE_TYPE} of the value with the specified key
	 * @throws ClassCastException If this JSON is not a JSON object
	 */
	public final JSONVALUE_TYPE json_getType(final String _key) throws ClassCastException {
		if (root_.type != JSONVALUE_TYPE.JSON_OBJECT) {
			throw new ClassCastException("JSON is not an object. JSON is '" + root_.toString() + "'");
		}
		JSONVALUE value = ((HashMap<String, JSONVALUE>) root_.value).get(_key);
		return (value.type);
	}

	/**
	 * Returns the value in this JSON with the given key. This assumes that this
	 * JSON is a JSON object. Returns null, if no element with the given key
	 * exists in the JSON object.
	 *
	 * @param _key The key of the value to be retrieved
	 * @return The value with the given key, or null
	 * @throws ClassCastException If this JSON is not a JSON object
	 */
	public final Object json_get(final String _key) throws ClassCastException {
		if (root_.type != JSONVALUE_TYPE.JSON_OBJECT) {
			throw new ClassCastException("JSON is not an object. JSON is '" + root_.toString() + "'");
		}
		JSONVALUE value = ((HashMap<String, JSONVALUE>) root_.value).get(_key);
		return (value != null ? value.toValue() : null);
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type. This assumes that this JSON is a JSON object.
	 *
	 * @param <T> The type of the value to be retrieved (Java native or
	 * {@link JSON})
	 * @param _key The key of the value to be retrieved
	 * @return The value with the given key cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON object OR the value
	 * can not be cast to the given type
	 */
	public final <T> T json_getCast(final String _key) throws ClassCastException {
		return ((T) json_get(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type. This assumes that this JSON is a JSON object.
	 *
	 * @param _key The key of the value to be retrieved
	 * @return The value with the given key cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON object OR the value
	 * can not be cast to the given type
	 */
	public final String json_getString(final String _key) throws ClassCastException {
		return (json_getCast(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type. This assumes that this JSON is a JSON object.
	 *
	 * @param _key The key of the value to be retrieved
	 * @return The value with the given key cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON object OR the value
	 * can not be cast to the given type
	 */
	public final int json_getInt(final String _key) throws ClassCastException {
		return (json_getCast(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type. This assumes that this JSON is a JSON object.
	 *
	 * @param _key The key of the value to be retrieved
	 * @return The value with the given key cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON object OR the value
	 * can not be cast to the given type
	 */
	public final long json_getLong(final String _key) throws ClassCastException {
		return (json_getCast(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type. This assumes that this JSON is a JSON object.
	 *
	 * @param _key The key of the value to be retrieved
	 * @return The value with the given key cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON object OR the value
	 * can not be cast to the given type
	 */
	public final double json_getDouble(final String _key) throws ClassCastException {
		return (json_getCast(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type. This assumes that this JSON is a JSON object.
	 *
	 * @param _key The key of the value to be retrieved
	 * @return The value with the given key cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON object OR the value
	 * can not be cast to the given type
	 */
	public final float json_getFloat(final String _key) throws ClassCastException {
		return (this.<Double>json_getCast(_key).floatValue());
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type. This assumes that this JSON is a JSON object.
	 *
	 * @param _key The key of the value to be retrieved
	 * @return The value with the given key cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON object OR the value
	 * can not be cast to the given type
	 */
	public final boolean json_getBoolean(final String _key) throws ClassCastException {
		return (json_getCast(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type. This assumes that this JSON is a JSON object.
	 *
	 * @param _key The key of the value to be retrieved
	 * @return The value with the given key cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON object OR the value
	 * can not be cast to the given type
	 */
	public final JSON json_getObject(final String _key) throws ClassCastException {
		JSON ret = json_getCast(_key);
		if (ret.root_.type != JSONVALUE_TYPE.JSON_OBJECT) {
			throw new ClassCastException(_key + " is not an object.");
		}
		return (ret);
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type. This assumes that this JSON is a JSON object.
	 *
	 * @param _key The key of the value to be retrieved
	 * @return The value with the given key cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON object OR the value
	 * can not be cast to the given type
	 */
	public final JSON json_getArray(final String _key) throws ClassCastException {
		JSON ret = json_getCast(_key);
		if (ret.root_.type != JSONVALUE_TYPE.JSON_ARRAY) {
			throw new ClassCastException(_key + " is not an array.");
		}
		return (ret);
	}

	/**
	 * Returns whether or not a value with the given key exists in this JSON.
	 * This assumes that this JSON is a JSON object or null.
	 *
	 * @param _key The key to check
	 * @return <i>True</i>, if this JSON is a JSON object and a value with the
	 * given key exists. <i>False</i> otherwise.
	 * @throws ClassCastException If this JSON is neither a JSON object nor null
	 */
	public final boolean json_containsKey(final String _key) throws ClassCastException {
		if (root_.type == JSONVALUE_TYPE.NULL) {
			return (false);
		}

		if (root_.type != JSONVALUE_TYPE.JSON_OBJECT) {
			throw new ClassCastException("JSON is not an object. JSON is '" + root_.toString() + "'");
		}

		return (((HashMap<String, JSONVALUE>) root_.value).containsKey(_key));
	}

	/**
	 * Removes the key/value pair with the given key from this JSON. This
	 * assumes that this JSON is a JSON object. If the given key is not present,
	 * nothing happens.
	 *
	 * @param _key The key to be removed
	 * @return This JSON
	 * @throws ClassCastException If this JSON is not a JSON object
	 */
	public final JSON json_removeKey(final String _key) throws ClassCastException {
		if (root_.type != JSONVALUE_TYPE.JSON_OBJECT) {
			throw new ClassCastException("JSON is not an object. JSON is '" + root_.toString() + "'");
		}

		((HashMap<String, JSONVALUE>) root_.value).remove(_key);
		return (this);
	}

	//#                                                                        #
	//#                                                                        #
	//+++++++++++++++++++++++++++Value == Array+++++++++++++++++++++++++++++++++
	/**
	 * Adds the given value to this JSON. This assumes that this JSON is either
	 * a JSON array or null. If this JSON is null, this JSON will be converted
	 * to a new empty JSON array. The value will be added to the end of the
	 * array.
	 *
	 * @param _value The new value
	 * @return This JSON
	 * @throws ClassCastException If this JSON is neither a JSON array nor null
	 */
	public final JSON json_add(final Object _value) throws ClassCastException {
		if (root_.type != JSONVALUE_TYPE.JSON_ARRAY) {
			if (root_.type != JSONVALUE_TYPE.NULL) {
				throw new ClassCastException("JSON is not an array. JSON is '" + root_.toString() + "'");
			}
			root_ = new JSONVALUE(JSONVALUE_TYPE.JSON_ARRAY, new ArrayList<JSONVALUE>());
		}

		JSONVALUE newvalue = new JSONVALUE(_value);
		((ArrayList<JSONVALUE>) root_.value).add(newvalue);

		return (this);
	}

	/**
	 * Adds the given value to this JSON.This assumes that this JSON is either a
	 * JSON array or null. If this JSON is null, this JSON will be converted to
	 * a new empty JSON array. The value will be inserted at the given position.
	 *
	 * @param _key The position to insert the new value into
	 * @param _value The new value
	 * @return This JSON
	 * @throws ClassCastException If this JSON is neither a JSON array nor null
	 */
	public final JSON json_add(final int _key, final Object _value) throws ClassCastException {
		if (root_.type != JSONVALUE_TYPE.JSON_ARRAY) {
			if (root_.type != JSONVALUE_TYPE.NULL) {
				throw new ClassCastException("JSON is not an array. JSON is '" + root_.toString() + "'");
			}
			root_ = new JSONVALUE(JSONVALUE_TYPE.JSON_ARRAY, new ArrayList<JSONVALUE>());
		}

		JSONVALUE newvalue = new JSONVALUE(_value);
		((ArrayList<JSONVALUE>) root_.value).add(_key, newvalue);

		return (this);
	}

	/**
	 * Sets the value at the given position in this JSON to the given value.This
	 * assumes that this JSON is a JSON array. The value will replace the value
	 * at the given position. This will only work if there is an element present
	 * at that position.
	 *
	 * @param _key The position to replace the value
	 * @param _value The new value
	 * @return This JSON
	 * @throws ClassCastException If this JSON is not a JSON array
	 * @throws IndexOutOfBoundsException If the there is no value at the
	 * specified position
	 */
	public final JSON json_set(final int _key, final Object _value) throws ClassCastException, IndexOutOfBoundsException {
		if (root_.type != JSONVALUE_TYPE.JSON_ARRAY) {
			throw new ClassCastException("JSON is not an array. JSON is '" + root_.toString() + "'");
		}

		JSONVALUE newvalue = new JSONVALUE(_value);
		((ArrayList<JSONVALUE>) root_.value).set(_key, newvalue);

		return (this);
	}

	/**
	 * Returns the {@link JSONVALUE_TYPE} of the value in this JSON with the
	 * specified key. This assumes that this JSON is a JSON array.
	 *
	 * @param _key The index of the value to check the type of
	 * @return The {@link JSONVALUE_TYPE} of the value with the specified index
	 * @throws ClassCastException If this JSON is not a JSON array
	 */
	public final JSONVALUE_TYPE json_getType(final int _key) throws ClassCastException {
		if (root_.type != JSONVALUE_TYPE.JSON_ARRAY) {
			throw new ClassCastException("JSON is not an array. JSON is '" + root_.toString() + "'");
		}
		JSONVALUE value = ((ArrayList<JSONVALUE>) root_.value).get(_key);
		return (value.type);
	}

	/**
	 * Returns the value in this JSON with the given key. This assumes that this
	 * JSON is a JSON array. Returns null, if no element with the given key
	 * exists in the JSON array.
	 *
	 * @param _key The index of the value to be retrieved
	 * @return The value with the given index, or null
	 * @throws ClassCastException If this JSON is not a JSON array
	 */
	public final Object json_get(final int _key) throws ClassCastException {
		if (root_.type != JSONVALUE_TYPE.JSON_ARRAY) {
			throw new ClassCastException("JSON is not an array. JSON is '" + root_.toString() + "'");
		}
		ArrayList<JSONVALUE> values = ((ArrayList<JSONVALUE>) root_.value);
		if (_key >= values.size()) {
			return (null);
		}
		JSONVALUE value = values.get(_key);
		return (value.toValue());
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type. This assumes that this JSON is a JSON array.
	 *
	 * @param <T> The type of the value to be retrieved (Java native or
	 * {@link JSON})
	 * @param _key The index of the value to be retrieved
	 * @return The value with the given index cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON array OR the value
	 * can not be cast to the given type
	 */
	public final <T> T json_getCast(final int _key) throws ClassCastException {
		return ((T) json_get(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type.This assumes that this JSON is a JSON array.
	 *
	 * @param _key The index of the value to be retrieved
	 * @return The value with the given index cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON array OR the value
	 * can not be cast to the given type
	 */
	public final String json_getString(final int _key) throws ClassCastException {
		return (json_getCast(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type.This assumes that this JSON is a JSON array.
	 *
	 * @param _key The index of the value to be retrieved
	 * @return The value with the given index cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON array OR the value
	 * can not be cast to the given type
	 */
	public final int json_getInt(final int _key) throws ClassCastException {
		return (json_getCast(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type.This assumes that this JSON is a JSON array.
	 *
	 * @param _key The index of the value to be retrieved
	 * @return The value with the given index cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON array OR the value
	 * can not be cast to the given type
	 */
	public final long json_getLong(final int _key) throws ClassCastException {
		return (json_getCast(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type.This assumes that this JSON is a JSON array.
	 *
	 * @param _key The index of the value to be retrieved
	 * @return The value with the given index cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON array OR the value
	 * can not be cast to the given type
	 */
	public final double json_getDouble(final int _key) throws ClassCastException {
		return (json_getCast(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type.This assumes that this JSON is a JSON array.
	 *
	 * @param _key The index of the value to be retrieved
	 * @return The value with the given index cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON array OR the value
	 * can not be cast to the given type
	 */
	public final float json_getFloat(final int _key) throws ClassCastException {
		return (this.<Double>json_getCast(_key).floatValue());
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type.This assumes that this JSON is a JSON array.
	 *
	 * @param _key The index of the value to be retrieved
	 * @return The value with the given index cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON array OR the value
	 * can not be cast to the given type
	 */
	public final boolean json_getBoolean(final int _key) throws ClassCastException {
		return (json_getCast(_key));
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type.This assumes that this JSON is a JSON array.
	 *
	 * @param _key The index of the value to be retrieved
	 * @return The value with the given index cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON array OR the value
	 * can not be cast to the given type
	 */
	public final JSON json_getObject(final int _key) throws ClassCastException {
		JSON ret = json_getCast(_key);
		if (ret.root_.type != JSONVALUE_TYPE.JSON_OBJECT) {
			throw new ClassCastException(_key + " is not an object.");
		}
		return (ret);
	}

	/**
	 * Returns the value in this JSON with the given key and casts it to the
	 * given type.This assumes that this JSON is a JSON array.
	 *
	 * @param _key The index of the value to be retrieved
	 * @return The value with the given index cast to the given type
	 * @throws ClassCastException If this JSON is not a JSON array OR the value
	 * can not be cast to the given type
	 */
	public final JSON json_getArray(final int _key) throws ClassCastException {
		JSON ret = json_getCast(_key);
		if (ret.root_.type != JSONVALUE_TYPE.JSON_ARRAY) {
			throw new ClassCastException(_key + " is not an array.");
		}
		return (ret);
	}

	/**
	 * Returns whether or not a value with the given key exists in this JSON.
	 * This assumes that this JSON is a JSON array or null.
	 *
	 * @param _key The index to check
	 * @return <i>True</i>, if this JSON is a JSON array and a value with the
	 * given index exists. <i>False</i> otherwise.
	 * @throws ClassCastException If this JSON is neither a JSON object nor null
	 */
	public final boolean json_containsKey(final int _key) throws ClassCastException {
		if (root_.type == JSONVALUE_TYPE.NULL) {
			return (false);
		}

		if (root_.type != JSONVALUE_TYPE.JSON_ARRAY) {
			throw new ClassCastException("JSON is not an array. JSON is '" + root_.toString() + "'");
		}

		return (((ArrayList<JSONVALUE>) root_.value).size() > _key);
	}

	/**
	 * Removes the value with the given index from this JSON. This assumes that
	 * this JSON is a JSON array. If the given index is not present, nothing
	 * happens.
	 *
	 * @param _key The index to be removed
	 * @return This JSON
	 * @throws ClassCastException If this JSON is not a JSON array
	 */
	public final JSON json_removeKey(final int _key) throws ClassCastException {
		if (root_.type != JSONVALUE_TYPE.JSON_ARRAY) {
			throw new ClassCastException("JSON is not an array. JSON is '" + root_.toString() + "'");
		}

		((ArrayList<JSONVALUE>) root_.value).remove(_key);
		return (this);
	}

	/**
	 * Returns the index of the first occurrence of the given value in this
	 * JSON.This assumes that this JSON is a JSON array or null. Returns -1 if
	 * the given value is not present or this JSON is null.
	 *
	 * @param _value The value to get the first index of
	 * @return The index of the first occurrence of the specified value OR -1
	 * @throws ClassCastException If this JSON is neither a JSON array nor null
	 */
	public final int json_indexOf(final Object _value) throws ClassCastException {
		if (root_.type == JSONVALUE_TYPE.NULL) {
			return (-1);
		}

		if (root_.type != JSONVALUE_TYPE.JSON_ARRAY) {
			throw new ClassCastException("JSON is not an array. JSON is '" + root_.toString() + "'");
		}

		ArrayList<JSONVALUE> list = ((ArrayList<JSONVALUE>) root_.value);
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).equalsValue(_value)) {
				return (i);
			}
		}

		return (-1);
	}

	/**
	 * Removes the first occurrence of the given value in this JSON. This
	 * assumes that this JSON is a JSON array or null. If the given value is not
	 * present in this JSON or this JSON is null, nothing happens.
	 *
	 * @param _value The value to remove
	 * @return This JSON
	 * @throws ClassCastException If this JSON is neither a JSON array nor null
	 */
	public final JSON json_removeValue(final Object _value) throws ClassCastException {
		int index = json_indexOf(_value);

		if (index == -1) {
			return (this);
		}

		((ArrayList<JSONVALUE>) root_.value).remove(index);

		return (this);
	}

	//#                                                                        #
	//#                                                                        #
	//++++++++++++++++++++++++Value == Collection+++++++++++++++++++++++++++++++
	/**
	 * Returns the number of entries in this JSON. This assumes that this JSON
	 * is either a JSON object, a JSON array or null. If this JSON is null, 0 is
	 * returned.
	 *
	 * @return The size of this JSON collection
	 * @throws ClassCastException If this JSON is neither a JSON object nor a
	 * JSON array nor null
	 */
	public final int json_size() throws ClassCastException {
		if (root_.type == JSONVALUE_TYPE.NULL) {
			return (0);
		}

		if (root_.type == JSONVALUE_TYPE.JSON_OBJECT) {
			return (((HashMap) root_.value).size());
		} else if (root_.type == JSONVALUE_TYPE.JSON_ARRAY) {
			return (((ArrayList) root_.value).size());
		}

		throw new ClassCastException("JSON is not a collection. JSON is '" + root_.toString() + "'");
	}

	/**
	 * Returns whether or not the given value is present in this JSON. This
	 * assumes that this JSON is either a JSON object, a JSON array or null. If
	 * this JSON is null, false is returned.
	 *
	 * @param _value The value to check the presence of
	 * @return <i>True</i>, if this is a JSON object or array and contains the
	 * given value. <i>False</i> otherwise.
	 * @throws ClassCastException If this JSON is neither a JSON object nor a
	 * JSON array nor null
	 */
	public final boolean json_containsValue(final Object _value) throws ClassCastException {
		switch (root_.type) {
			case NULL:
				return (false);
			case JSON_ARRAY:
				for (JSONVALUE v : ((ArrayList<JSONVALUE>) root_.value)) {
					if (v.equalsValue(_value)) {
						return (true);
					}
				}
				return (false);
			case JSON_OBJECT:
				for (Map.Entry<String, JSONVALUE> entry : ((HashMap<String, JSONVALUE>) root_.value).entrySet()) {
					if (entry.getValue().equalsValue(_value)) {
						return (true);
					}
				}
				return (false);
			default:
				throw new ClassCastException("JSON is not a collection. JSON is '" + root_.toString() + "'");
		}
	}

	//==========================================================================
	//#                                                                        #
	//#                       Public Output API                                #
	//#                                                                        #
	//==========================================================================
	@Override
	public String toString() {
		return (json_toString(true));
	}

	/**
	 * Converts this JSON into a parsable String representation with or without
	 * added white space.
	 *
	 * @param _whitespace Whether or not white space shall be used to render the
	 * output human readable.
	 * @return This JSON as string
	 */
	public final String json_toString(final boolean _whitespace) {
		return (root_.json_toString(_whitespace, 1));
	}

	/**
	 * Converts this JSON into a parsable String representation with or without
	 * added white space and streams the result to a given stream.
	 *
	 * @param _destination The stream to write the content to
	 * @param _bufferSize The buffer size
	 * @param _whitespaces Whether or not white space shall be used to render
	 * the output human readable.
	 * @return This JSON
	 * @throws IOException If stream writing fails
	 */
	public final JSON json_writeToStream(final OutputStream _destination, final int _bufferSize, final boolean _whitespaces) throws IOException {
		byte[] buffer = new byte[_bufferSize];
		ByteBuffer content = ByteBuffer.wrap(json_toString(_whitespaces).getBytes());
		while (content.hasRemaining()) {
			int readBytes = Math.min(buffer.length, content.remaining());
			content.get(buffer, 0, readBytes);
			_destination.write(buffer, 0, readBytes);
			_destination.flush();
		}
		return (this);
	}

	/**
	 * Converts this JSON into a parsable String representation with or without
	 * added white space and streams the result to a given file.
	 *
	 * @param _destination The file to write the content to
	 * @param _bufferSize The buffer size
	 * @param _whitespaces Whether or not white space shall be used to render
	 * the output human readable.
	 * @return This JSON
	 * @throws IOException If stream writing fails
	 */
	public final JSON json_writeToFile(final File _destination, final int _bufferSize, final boolean _whitespaces) throws IOException {
		try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(_destination))) {
			json_writeToStream(bos, _bufferSize, _whitespaces);
		}
		return (this);
	}

}
